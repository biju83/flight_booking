#Flight Reservation System

    Getting Started:-
    Given instruction will help you set the project in your pc. 

#steps to follow:-

    create virtual environment
    start project 
    start app

#Softwares need to be installed:-

    python 2.7
    django 1.11
    postgre sql

#Api's implemented:-

1. give source-city and destination city will provide flight_name with flight_id.
   link is:   http://127.0.0.1:8000/get1/?src=%27begaluru%27&dst=%27delhi%27

2. create booking using post in postman 

3. provide user_id as email_id will show booking details
   link is: http://127.0.0.1:8000/get3/?email=bijendrakumar255@gmail.com

4. given airline_name details find airline details for book tickets
   link is: http://127.0.0.1:8000/get3/indigo    
