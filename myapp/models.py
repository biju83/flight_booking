# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
from decimal import Decimal


class Flight(models.Model):


    class Meta:
        verbose_name_plural='flight'

    flight_id = models.IntegerField(primary_key=True)
    airline_name = models.CharField(max_length=50,default='null')
    airline_id = models.CharField(max_length=50,default='null')
    source = models.CharField(max_length=50,default='null')
    source_code = models.CharField(max_length=50,default='null')
    destination = models.CharField(max_length=50,default='null')
    destination_code = models.CharField(max_length=50,default='null')
    departure_time = models.DateTimeField(null=True)
    arrival_time = models.DateTimeField(null=True)
    total_seats = models.IntegerField()

    def __str__(self):
        return str(self.airline_id)



class FlightDetails(models.Model):

    class Meta:
        verbose_name_plural='flight_details'

    flight_id = models.ForeignKey(Flight)
    departure_date = models.DateField(null=True)
    price = models.DecimalField(max_digits=20,decimal_places=4,default=Decimal('0.0000'))
    available_seats = models.IntegerField()
    def __str__(self):
        return str(self.departure_date)



class PassengerDetails(models.Model):


    class Meta:
        verbose_name_plural='passenger_details'

    first_name = models.CharField(max_length=55,default='null')
    last_name = models.CharField(max_length=55,default='null')
    age = models.IntegerField()
    passport_id = models.CharField(max_length=55,default='null')
    address = models.CharField(max_length=55,default='null')
    phone_number =models.CharField(max_length=55,default='null')
    email = models.EmailField(max_length=55,primary_key=True) ###userid

    def __str__(self):
        return self.first_name


class TicketDetails(models.Model):


    class Meta:
        verbose_name_plural='ticket_details'

    email = models.ForeignKey(PassengerDetails)
    ticket_id = models.IntegerField(primary_key=True)
    flight_id = models.ForeignKey(Flight)
    departure_date = models.ForeignKey(FlightDetails)
    status = models.CharField(max_length=55,default='null')



class CardDetails(models.Model):

    class Meta:
        verbose_name_plural='card_details'

    email = models.ForeignKey(PassengerDetails)
    bank_name = models.CharField(max_length=55,default='null')
    card_type = models.CharField(max_length=55,default='null')
    card_number = models.CharField(max_length=16)
    cvv = models.IntegerField()
    expiration_date = models.DateField(null=True)








# Create your models here.
