# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import View

from django.shortcuts import render
from django.http import HttpResponse,QueryDict
from django.core.exceptions import ObjectDoesNotExist
from  myapp.models import Flight,FlightDetails,TicketDetails,PassengerDetails,CardDetails



class API_1(View):

    def get(self,request):
        source = request.GET.get('src', '0')
        destination = request.GET.get('dst', '0')
        if source == '0' or destination == '0':
            return HttpResponse('please find source and destination')

        result=[]
        data=Flight.objects.filter(source=source,destination=destination)
        obj=Flight.objects.all()
        for i in obj:
                result.append(i.airline_id)
                result.append("    ")
                result.append(i.airline_name)
                result.append("<br>")

        return HttpResponse(result)



class API_3(View):
    def get(self,request):
        email=request.GET.get('email')

        result=[]
        data=TicketDetails.objects.filter(email=email)


        #print (data)


        # obj=PassengerDetails.objects.all()
        #a = select * from TicketDetails where TicketDetails.email_id = PassengerDetails.email_id
        #print(a.ticket_id)
        for i in data:
            #obj=FlightDetails.objects.filter(departure_date=i.departure_date)

            result.append(i.email)
            result.append("  flight_id  -  ")
            result.append(i.flight_id)
            result.append("   ticket_id  -  ")
            result.append(i.ticket_id)
            result.append("  departure_date  -  ")
            result.append(i.departure_date)
            result.append("  status  -  ")
            result.append(i.status)
            result.append("<br>")

        return HttpResponse(result)



class API_4(View):

    def get(self,request,airline_name):
        result=[]
        data=Flight.objects.filter(airline_name=airline_name)
        obj=Flight.objects.all()
        for i in data:
                result.append(i.airline_id)
                result.append("    src->  ")
                result.append(i.source)
                result.append("   des->  ")
                result.append(i.destination)
                result.append("   depart_time->  ")
                result.append(i.departure_time)
                result.append("   arrival_time->  ")
                result.append(i.arrival_time)
                result.append("   total_seats->  ")
                result.append(i.total_seats)
                result.append("<br>")

        return HttpResponse(result)

class Create(View):
    def post(self,request):
        first_name=request.POST["first_name"]
        last_name=request.POST["last_name"]
        age=request.POST["age"]
        passport_id=request.POST["passport_id"]
        address=request.POST["address"]
        phone_number=request.POST["phone_number"]
        email=request.POST["email"]
        data=PassengerDetails(first_name=first_name,last_name=last_name,age=age,passport_id=passport_id,
        address=address,phone_number=phone_number,email=email)
        data.save()
        return HttpResponse("Passenger Added")
