# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from myapp.models import Flight,FlightDetails,PassengerDetails,TicketDetails,CardDetails

admin.site.register(Flight)
admin.site.register(FlightDetails)
admin.site.register(PassengerDetails)
admin.site.register(TicketDetails)
admin.site.register(CardDetails)
# Register your models here.
