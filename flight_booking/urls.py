"""flight_booking URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from myapp.views import API_1,API_4,API_3,Create

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^get1/', API_1.as_view(), name='h'),
    url(r'^get3/',API_3.as_view(),name='p'),

    #url(r'^get1/(?P<source>\D+)/(?P<destination>\D+)',API_1.as_view(),name='r'),
    url(r'^get4/(?P<airline_name>\D+)',API_4.as_view(),name='h'),
    url(r'^create/',Create.as_view(),name='bij'),
    #url(r'^get2/', API_1.as_view(), name='c'),


]
